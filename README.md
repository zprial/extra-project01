# Hello

如果想在服务器打开本项目，请将`build`文件夹整个复制到静态资源目录下，然后即可通过您的静态资源服务器访问

假设您的域名是: https://www.example.com, 静态资源路径是: /static, 那么访问的路径即是： https://www.example.com/static/build/index.html

> 当然,build 文件夹名称可以随便改

## 项目运行

以下操作请确保您的电脑已安装`NodeJS`，如果未安装，请到官网下载[NodeJS](https://nodejs.org/en/),推荐 LTS 版本。

```bash
 $ npm install # 初次运行，安装相关依赖
 $ npm start # 开始开发
 $ npm run build # 编译打包，该命令会编译打包代码到 build 文件夹下
```

## 数据获取

项目自带了一份不太全的mock数据，如果想使用接口获取数据，请修改`public/index.html`文件里的第46行：

```javascript
  window._fetchDataApi = "您的接口地址"; // 数据格式请按照文件 src/stores/mock.js 的格式来即可
```
