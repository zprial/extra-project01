const fs = require('fs')

let mockData = require('./src/stores/mock');

// 交换纬度比精度大的
function swipLatAndLon(data = []) {
  for (let team of data) {
    if (team.latitude && team.longitude && +team.latitude > +team.longitude) {
      let _latitude = team.latitude
      team.latitude = team.longitude
      team.longitude = _latitude
    } else if (team.sons && team.sons.length > 0) {
      swipLatAndLon(team.sons)
    }
  }
}
swipLatAndLon(mockData)

fs.writeFile('./mockdata.json', JSON.stringify(mockData), {encoding: 'utf8'}, (error) => {
  console.log('error:', error)
})