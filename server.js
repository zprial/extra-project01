const Koa = require('koa');
const bodyParser = require('koa-bodyparser');
const cors = require('koa-cors');
const koaStatic = require('koa-static');
const path = require('path');
const Router = require('koa-router');

const mockData = require('./src/stores/mock');

const app = new Koa();
const router = new Router();

app.use(cors());
app.use(bodyParser());
app.use(koaStatic(path.join(__dirname, './build')));

router.get('/api/mock', (ctx) => {
  ctx.body = mockData
})

app.use(router.routes());

app.listen(3010);

app.on('error', (error) => {
  console.warn('app has some error:', error);
});
