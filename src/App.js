import React, { Component } from 'react';
import List from './components/List';
import Map from './components/Map';
import Toast from './components/Toast';
import { GLOBAL_DATA_ENUM, MAP_ZOOM_ENUM } from './enum';
import { getZhiduiData } from './utils';

const { __TEAM_DATA, __TEAM_ZHIDUI } = GLOBAL_DATA_ENUM;

const TEAM_PROVINCE = {
  id: '00000',
  name: '浙江省消防总队',
  toggled: true
}

class App extends Component {

  _mapRef = null;
  _toggledNum = 1;
  _toggledZhiduiNum = 0; // 展开的支队数量
  state = {
    teams: [TEAM_PROVINCE],
    // 当前选中的有子消防队的消防大队或者消防支队
    currentTeam: {}
  }

  componentDidMount() {
    if (global._fetchDataApi) {
      this.onFetchData(global._fetchDataApi);
    } else {
      Toast.info('获取数据中...', 0);
      import('./stores/mock').then(({ default : result }) => {
        Toast.hide();
        if (Array.isArray(result)) {
          global[__TEAM_DATA] = [{
            ...TEAM_PROVINCE,
            sons: result
          }];
          // 保存支队数据
          global[__TEAM_ZHIDUI] = getZhiduiData(result);
          this.setState({
            teams:  [{
              ...TEAM_PROVINCE,
              sons: result
            }]
          });
        }
      }).catch(error => {
        Toast.info('数据获取失败了:', error.message)
      })
    }
  }

  onItemClick = (team) => {
    const map = this._mapRef._map;
    const point = new window.BMap.Point(team.longitude, team.latitude);
    map.setCenter(point);
    map.setZoom(15);
    const mark = this._mapRef._markers.find(m => m._id === team.id);
    if (mark) {
      map.removeOverlay(mark);
      map.addOverlay(mark);
      setTimeout(() => {
        this._mapRef.isAutoTrigger = true;
        mark._infoWindow.open(mark.point);
      }, 450);
    }
  }

  onToggle = (node, toggled) => {
    if (node.id === '00000' && !toggled) {
      this._mapRef.renderZhiduiInfo();
      return;
    }
  
    if (toggled) {
      this._mapRef.isAutoTrigger = true;
    }
  
    // 针对支队特殊处理
    if (node.name && node.name.match(/支队$/)) {
      if (toggled) {
        this._toggledZhiduiNum++;
        this._mapRef.centerAndZoom(new window.BMap.Point(node.longitude, node.latitude), MAP_ZOOM_ENUM.LEVEL01);
        return;
      } else {
        this._toggledZhiduiNum--;
      }
      // 如果没有展开的支队，那么展示所有支队的统计数据
      if (this._toggledZhiduiNum <= 0) {
        this._mapRef.renderZhiduiInfo();
        return;
      }
      return;
    }
  
    toggled ? this._toggledNum++ : this._toggledNum--;

    // 有children的大队点击
    if (toggled && node.name && node.name.match(/大队$/) && node.children) {
      if (node.id !== this.state.currentTeam.id) {
        if (node.longitude && node.latitude) {
          this._mapRef.centerAndZoom(new window.BMap.Point(node.longitude, node.latitude), MAP_ZOOM_ENUM.LEVEL02);
        }
        // else {
          // this.setState({
          //   currentTeam: node
          // })
        // }
      }
    }
    // 前提是有经纬度
    // 不是大队或者没有children(多此一举了？)
    else if ((!node.name.match(/大队$/) || !node.children) && node.longitude && node.latitude) {
      const map = this._mapRef._map;
      const point = new window.BMap.Point(node.longitude, node.latitude);
      map.setCenter(point);
      map.setZoom(15);
      const mark = this._mapRef._markers.find(m => m._id === node.id);
      if (mark) {
        map.removeOverlay(mark);
        map.addOverlay(mark);
        setTimeout(() => {
          this._mapRef.isAutoTrigger = true;
          mark._infoWindow.open(mark.point);
        }, 450);
      }
    } else if((!node.longitude || !node.latitude) && !node.children) {
      Toast.info('非常抱歉，未能获取到该消防站的经纬度~');
    }

    // 如果没有展开的了，那就展示全部的
    if (this._toggledNum <= 0) {
      this.setState({
        currentTeam: {}
      })
    }
  }

  // 通过文件导入
  onSelectFile = (file) => {
    const _self = this;
    if (file) {
      const fs = new FileReader()
      fs.onloadend = function(e) {
        try {
          const data = JSON.parse(e.target.result);
          
          _self.setState({
            teams: [{
              ...TEAM_PROVINCE,
              sons: data
            }] 
        });
        } catch (error) {
          Toast.info('导入数据失败:\n' + error.message);
        }
      }
      fs.readAsText(file);
    }
  }

  // 通过接口查询数据
  onFetchData = (url) => {
    if (/^https?:\/\//ig.test(url)) {
      Toast.info('获取数据中...', 0);
      fetch(url).then(resp => resp.json()).then(result => {
        Toast.hide();
        if (Array.isArray(result)) {
          global[__TEAM_DATA] = [{
            ...TEAM_PROVINCE,
            sons: result
          }];
          // 保存支队数据
          global[__TEAM_ZHIDUI] = getZhiduiData(result);
          this.setState({
            teams: [{
              ...TEAM_PROVINCE,
              sons: result
            }] 
          });
        }
      }).catch(err=> {
        Toast.info('获取数据失败:' + err.message);
      })
    } else {
      Toast.info('请输入正确的请求地址~');
    }
  }

  render() {
    const { teams, currentTeam } = this.state
    return (
      <div className="App">
        <List onItemClick={this.onItemClick} onFetchData={this.onFetchData} onSelectFile={this.onSelectFile} data={teams} onToggle={this.onToggle} />
        <Map ref={n => this._mapRef = n} teams={currentTeam.children || teams} />
      </div>
    );
  }
}

export default App;
