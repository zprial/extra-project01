import React, { Component } from 'react';
import classnames from 'classnames'
import { Treebeard, decorators } from 'react-treebeard';
import Toast from '../Toast';
import SearchBar from '../SearchBar';
import { GLOBAL_DATA_ENUM } from '../../enum';
import { getAllTeams } from '../../utils'

import './index.scss';

const { __TEAM_ZHIDUI } = GLOBAL_DATA_ENUM;

const logo = require('../../images/logo.png');

decorators.Toggle = (props) => {
  if (props.node.id === '00000') {
    return <img src={logo} alt="" width="40" />
  }
  if (props.node.children && props.node.children.length > 0) {
    return <i className={classnames("iconfont icon-triangle", {
      rotate: props.node.toggled
    })} />
  }
  return null;
}
decorators.Header = (props) => {
  const ZHIDUI = global[__TEAM_ZHIDUI];
  const target = (ZHIDUI || []).find(n => n.id === props.node.id);
  return <span className={classnames("tree-list_item", {
    disabled: !props.node.children && (!props.node.longitude || !props.node.latitude),
    'tree-list_item-toggled': props.node.toggled
  })} style={props.style}>
    {props.node.name}
    {target && target.amount ? `(${target.amount})`: ''}
    {!target && props.node.id !== "00000" && props.node.children && `(${getAllTeams(props.node.children).length})`}
  </span>
}
decorators.Container = (props) => <div onClick={props.onClick} className={classnames({
  'tree-list--main': props.node.id === "00000"
})} >
  <props.decorators.Toggle {...props}/>
  <props.decorators.Header {...props}/>
</div>

function setAllTeams(data = []) {
  for (let team of data) {
    if (team.sons && team.sons.length > 0) {
      team.children = team.sons;
      setAllTeams(team.sons);
    }
  }
  return data;
}

export default class extends Component {
  static defaultProps = {
    data: [] // 消防站数据
  }

  state = {
    data: [],
    cursor: null,
    hidePanel: false, // 是否隐藏panel
    urlValue: window.location.origin + '/api/mock', // 数据请求接口
    showImportData: false, // 是否展示数据导入
  }

  componentDidMount() {
    const { data } = this.props;
    const datas = setAllTeams(data);
    let showImportData = false;
    if (~window.location.search.indexOf('showImportData')) {
      showImportData = true;
    }
    this.setState({
      data: datas,
      showImportData
    })
  }

  componentWillReceiveProps(nextProps) {
    const { data } = nextProps;
    const datas = setAllTeams(data);
    this.setState({
      data: datas
    })
  }

  onToggle = (node, toggled) => {
    if (node.id === '00000' && !toggled) {
      this.props.onToggle(node, toggled)
      return;
    }
    node.active = true;
    if(node.children){
      node.toggled = toggled;
    }
    this.setState({ cursor: node });
    if (this.props.onToggle) {
      this.props.onToggle(node, toggled)
    }
  }

  // 切换显示panel
  togglePanel = () => {
    this.setState({
      hidePanel: !this.state.hidePanel
    })
  }

  // 数据导入
  onFileChange = (e) => {
    if (!window.FileReader) {
      Toast.info('抱歉，您的浏览器不支持该特性，推荐使用最新的Chrome浏览器~');
      return;
    }
    const file = e.target.files[0];
    const { onSelectFile } = this.props;
    if (onSelectFile) {
      onSelectFile(file)
    }
  }

  onChangeUrl = (e) => {
    this.setState({
      urlValue: e.target.value.trim()
    })
  }

  // 请求数据
  onFetchData = () => {
    const { onFetchData } = this.props;
    if (onFetchData) {
      onFetchData(this.state.urlValue);
    }
  }

  render() {
    const { hidePanel, data, showImportData } = this.state
    return <section className={`tree-list ${hidePanel ? 'hidePanel' : ''}`}>
      <div>
        <SearchBar data={data} onItemClick={this.props.onItemClick} />
        <div className="tree-list_container">
          {showImportData && <div className="tree-list_cooperation">
            <div className="input">
              <input type="search" placeholder="请输入接口地址" value={this.state.urlValue} onChange={this.onChangeUrl} />
              <input className="button submit" type="submit" value="接口获取数据" onClick={this.onFetchData} />
              <div className="file">
                <label htmlFor="file" className="button">导入JSON数据</label>
                <input type="file" accept="text/plain,application/json" id="file" onChange={this.onFileChange} />
              </div>
            </div>
          </div>}
          {data.length > 0
            ? <Treebeard data={data} onToggle={this.onToggle} decorators={decorators} />
            : <div className="tree-list_empty">
              <img src={require('../../images/img_nodata.png')} alt="empty" />
              <p>空空如也~</p>
            </div>}
        </div>
      </div>
      <span className="tree-list_btn" onClick={this.togglePanel}><i className="iconfont icon-left" /></span>
    </section>
  }
}