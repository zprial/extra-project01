import React, { Component } from 'react';
import debounce from 'lodash/debounce';
import { getAllTeams } from '../../utils';
import { ICON_FIRE_HYDRANT, TEAM_TYPE_ENUM, GLOBAL_DATA_ENUM, MAP_ZOOM_ENUM } from '../../enum';
import './index.scss';

const { __TEAM_ZHIDUI, __TEAM_DATA } = GLOBAL_DATA_ENUM;

// 是否是第一次进来
let isFirstEntry = true;

// 记录上一次的缩放等级
let lastZoom = MAP_ZOOM_ENUM.ZHIDUI;

const icons = []
export default class extends Component {
  static defaultProps = {
    teams: []
  }

  _map = null;
  _markerClusterer = null;
  _markers = [];

  // 是否自动触发移动缩放等
  isAutoTrigger = false;
  componentDidMount() {
    const BMap = window.BMap;
    let map = new BMap.Map(this.mapRef);
    this._map = map;
    const point = new BMap.Point(120.189639,30.24809);  // 创建点坐标  
    map.centerAndZoom(point, 8);
    map.enableScrollWheelZoom(true); // 允许鼠标滚轮缩放
    map.addControl(new BMap.NavigationControl({
      anchor: 3
    }));
    map.addControl(new BMap.ScaleControl());
    map.addControl(new BMap.OverviewMapControl());    
    map.addControl(new BMap.MapTypeControl());

    // 监听事件
    map.addEventListener('moveend', this.onMoveOrZoomEnd)
    map.addEventListener('zoomend', this.onMoveOrZoomEnd)

    // 缓存地图icon
    Object.keys(ICON_FIRE_HYDRANT).forEach(key => {
      icons.push(new BMap.Icon(ICON_FIRE_HYDRANT[key], new BMap.Size(30, 30), {
        imageSize: new BMap.Size(30, 30)
      }))
    })

    this.markPointInMap();
  }

  // 地图移动结束
  onMoveOrZoomEnd = debounce((e) => {
    if (this.isAutoTrigger) {
      this.isAutoTrigger = false;
      return;
    }
    const zoom = e.target.getZoom();
    // 展示支队就够
    if (zoom <= MAP_ZOOM_ENUM.ZHIDUI) {
      if (lastZoom !== zoom) {
        lastZoom = zoom;
      } else {
        this.isAutoTrigger = true;
      }
      this.renderZhiduiInfo();
    }
    // 展示所有范围内的消防队
    else if (zoom >= MAP_ZOOM_ENUM.TEAMS) {
      const bounds = e.target.getBounds();
      // 右上角经纬度
      const NE = bounds.getNorthEast();
      // 左下角经纬度
      const SW = bounds.getSouthWest();
      // 筛选范围内的消防队
      let count = 0;
      const teams = [];
      for (let marker of this._markers) {
        // 视口内最多给他展示1000个
        // if (count > 1000) {
        //   break;
        // }
        const position = marker.getPosition();
        if (position.lng <= NE.lng && position.lat <= NE.lat && position.lng >= SW.lng && position.lat >= SW.lat) {
          count++;
          teams.push(marker);
        }
      }
      this.markPointInMap(teams, true, false);
    }
  }, 300, { leading: true })

  // 初始化所有的markers,便于搜用使用
  initTotalMarks = () => {
    const teams = getAllTeams(global[__TEAM_DATA]);
    teams.forEach(team => {
      const [marker] = this.markOneTeam(team);
      if (marker) {
        this._markers.push(marker);
      }
    });
  }

  // 标记单个消防点信息
  markOneTeam(team) {
    const BMap = window.BMap;
    const map = this._map;
    const _self = this;
    if (team.longitude && team.latitude) {
      const content = `
        <ul class="info-window">
          <li>队伍地址: ${team.team_address}</li>
          <li>队伍类型: ${TEAM_TYPE_ENUM[team.team_type] || ''}</li>
          ${team.team_captial ? `<li>队长姓名: ${team.team_captial}</li>` : ''}
          ${team.team_tel ? `<li>队长电话: <a href="tel:${team.team_tel}">${team.team_tel}</a></li>` : ''}
        </ul>
      `;
      const point = new BMap.Point(team.longitude, team.latitude);
      const marker = new BMap.Marker(point, {
        icon: icons[team.team_type - 1] || icons[0]
      });
      marker._id = team.id;
      marker._infoWindow = new window.BMapLib.SearchInfoWindow(map, content, {
        title: team.name,
        width: 290,
        searchTypes: [
          window.BMAPLIB_TAB_SEARCH,
          window.BMAPLIB_TAB_TO_HERE
        ]
      });
      marker.addEventListener('click', function(e) {
        _self.isAutoTrigger = true;
        this._infoWindow.open(e.point)
      });
      return [marker, point];
    }
    return []
  }

  // 在地图上标记点
  // isMarker：teams是否已经是marker了
  // isNeedSetView：是否需要自动居中
  markPointInMap = (teamsData = this.props.teams, isMarker = false, isNeedSetView = true) => {
    const map = this._map;
    map.clearOverlays()
    const BMap = window.BMap;
    const teams = isMarker ? teamsData : getAllTeams(teamsData);
    const markers = [];
    const pointers = [];
    teams.forEach(team => {
      let marker = null;
      if (team instanceof BMap.Marker) {
        marker = team;
      } else {
        marker = this._markers.find(mark => mark._id === team.id);
      }
      if (marker) {
        markers.push(marker);
        pointers.push(marker.point);
      }
    });
    if (!this._markerClusterer) {
      const size = new BMap.Size(65, 65);
      this._markerClusterer = new window.BMapLib.MarkerClusterer(map, {
        markers: markers,
        minClusterSize: 5,
        styles: [{
          url: require('../../images/map-sp-mark-yellow@2x.png'),
          size: size,
          textSize: 20,
          textColor: '#fff',
        }, {
          url: require('../../images/map-sp-mark-blue@2x.png'),
          size: size,
          textSize: 20,
          textColor: '#fff'
        }, {
          url: require('../../images/map-sp-mark-red@2x.png'),
          size: size,
          textSize: 20,
          textColor: '#fff'
        }]
      });
    } else {
      this._markerClusterer.clearMarkers();
      this._markerClusterer.addMarkers(markers)
    }
    if (isNeedSetView) {
      map.setViewport(pointers, {
        margins: [0, 0, 0, 0],
      });
      this.isAutoTrigger = true;
    }
  }

  // 只展示指定坐标点的地图
  renderOnluPointers(teams = [], style = {}, onLabelClick) {
    const _self = this;
    const pointers = [];
    const map = this._map;
    const BMap = window.BMap;
    map.clearOverlays();
    if (this._markerClusterer) {
      this._markerClusterer.clearMarkers();
    }

    (teams || []).forEach(team => {
      if (team.longitude && team.latitude) {
        const point = new BMap.Point(team.longitude, team.latitude);
        pointers.push(point);
        if (team.amount > 5) {
          const label = new BMap.Label(team.amount, {
            position: point,
            offset: new BMap.Size(-32, -32) 
          });
          label.setStyle({
            color: "#fff",
            fontSize: '16px',
            fontWeight: 'bold',
            width: '65px',
            height: '65px',
            textAlign: 'center',
            lineHeight: '65px',
            backgroundColor: 'rgba(255, 0, 0, 0.65)',
            border: 'none',
            borderRadius: '50%',
            cursor: 'pointer',
            ...style
          });
          label.addEventListener('click', function() {
            onLabelClick && onLabelClick(team);
          });
          map.addOverlay(label);
        } else {
          // 直接渲染mark点
          team.children.forEach(team => {
            const marker = this._markers.find(mark => mark._id === team.id);
            if (marker) {
              map.addOverlay(marker);
            }
          });
        }
      }
    });
    map.setViewport(pointers, {
      margins: [0, 0, 0, 0],
    });
  }

  // 只展示支队点
  renderZhiduiInfo = (teams = global[__TEAM_ZHIDUI]) => {
    this.renderOnluPointers(teams, {}, (team) => {
      this.centerAndZoom(new window.BMap.Point(team.longitude, team.latitude), MAP_ZOOM_ENUM.LEVEL01);
    });
  }

  centerAndZoom = (point, level) => {
    this._map.centerAndZoom(point, level);
  }

  componentWillReceiveProps(nextProps) {
    if (isFirstEntry) {
      this.initTotalMarks();
      this.renderZhiduiInfo();
      isFirstEntry = false;
    } else {
      this.markPointInMap(nextProps.teams);
    }
  }

  render() {
    return <div id="map" ref={n => this.mapRef = n} />
  }
}