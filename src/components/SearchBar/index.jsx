import React from 'react';
import debounce from 'lodash/debounce';

import './index.scss';

// 筛选数据，队名满足参数
function filterTeamByName(data = [], teamName) {
  let teams = [];
  if (!teamName) {
    return teams;
  }
  for (let team of data) {
    if (team.name && !team.sons && ~team.name.indexOf(teamName)) {
      teams.push(team);
    } else if (team.sons && team.sons.length > 0) {
      teams = teams.concat(filterTeamByName(team.sons, teamName));
    }
    if (teams.length >= 10) {
      return teams;
    }
  }
  return teams;
}

export default class extends React.Component{
  state = {
    searchValue: '',
    isFocus: false,
    filterTeams: []
  }

  onChange = (e) => {
    e.persist();
    const value = e.target.value;
    this.setState({
      searchValue: value.trim()
    })
    this.changeHandler(e);
  }
  
  changeHandler = debounce(e => {
    const value = e.target.value;
    const filterTeams = filterTeamByName(this.props.data || [], value);
    this.setState({
      filterTeams: filterTeams.slice(0, 10)
    })
  }, 300);

  onItemClick = (team) => {
    if (!team.longitude || !team.latitude) {
      return;
    }
    const { onItemClick } = this.props;
    onItemClick && onItemClick(team);
  }

  onBlur = () => {
    setTimeout(() => {
      this.setState({
        isFocus: false
      })
    }, 300);
  }

  onFocus = () => {
    this.setState({
      isFocus: true
    })
  }
  
  render() {
    const { filterTeams, isFocus } = this.state;
    return <div className="searchbar">
      <div className="searchbar-input">
        <input
          type="text"
          placeholder="输入队伍名称搜索"
          onChange={this.onChange}
          onBlur={this.onBlur}
          onFocus={this.onFocus}
          value={this.state.searchValue}
        />
        {/* <p>* 搜索结果已过滤无经纬度信息的消防队</p> */}
      </div>
      {isFocus && filterTeams.length >0 && <ul className="searchbar-list">
        {filterTeams.map(team => <li
          className={!team.longitude || !team.latitude ? 'disabled' : ''}
          key={team.id}
          onClick={() => this.onItemClick(team)}
        >{team.name}</li>)}
      </ul>}
    </div>
  }
}