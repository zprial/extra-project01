// Toast组件
// 支持Toast.info(options) 方式
import React from 'react'
import classnames from 'classnames'
import { render, destroy } from '../internal/Modal/modalHelper'
import ClassAnimate from '../internal/Modal/classAnimate'
import './index.scss'

// 渲染Toast内容
const ToastInner = function (props) {
  const {
    className,
    content,
    position,
    isModal,
    zIndex,
    children,
    onHide,
    ...ps
    } = props
  return <div
    style={{ zIndex }}
    className={classnames(
      className,
      'dui-toast',
      `dui-toast--${position || 'top'}`, {
        'dui-toast-modal': !!isModal,
      })}
  >
    <div className="dui-toast-container" style={{ zIndex }}>
      <div {...ps} className="dui-toast-wrap">
        {content}
        {children}
      </div>
    </div>
  </div>
}

export default class Toast extends React.Component {
  // 当前通过方法调用的渲染元素(在该元素上渲染)数组
  static _renderNodes = []
  // 当前通过方法调用所展示的toast组件
  static _classAnimateDom = []

  static defaultProps = {
    position: 'top',
    duration: 300, // 动画持续时间
    timeout: 1500,  // 存在时间，timeout毫秒后自动销毁
    zIndex: 201,
  }

  /**
   * 处理传入的参数，转成指定格式的对象
   * @param option 传入的参数
   * @param timeout 持续的时间
   * @param callBack 回调方法
   */
  static _getProps(option, timeout, callBack) {
    let props = { ...this.defaultProps }
    if (typeof timeout === 'number') {
      props.timeout = timeout
    } else if (typeof timeout === 'function') {
      props.onHide = timeout
    }
    if (typeof callBack === 'function') {
      props.onHide = callBack
    }
    if (typeof option === 'string') {
      props = {
        ...props,
        content: option,
      }
    } else {
      props = {
        ...props,
        ...option,
      }
    }
    return props
  }

  // 隐藏组件
  static async hide(soft = true, callBack) {
    let _classAnimateDom = Toast._classAnimateDom.shift()
    let currentToastDOM = Toast._renderNodes.shift()
    if (soft) {
      if (_classAnimateDom) {
        // 此处false为禁止再次触发onExit
        await _classAnimateDom.exitHandler(false)
      }
      currentToastDOM && destroy(currentToastDOM)
    } else {
      // 销毁组件，移除节点
      currentToastDOM && destroy(currentToastDOM)
      // 如果连续调用toast会出现多个Toast的BUG，此处暂时停用
      _classAnimateDom = null
      // if (_classAnimateDom) {
      //   await _classAnimateDom.exitHandler(false)
      // }
    }
    if (typeof callBack === 'function') {
      callBack()
    }
  }

  /**
   * @description 基础提示
   * @param {ToastProps | String} option Toast配置
   * @param {number | function} timeout 定时，如果是个函数，则是关闭时回调
   * @param {function} callBack 关闭回调，如果timeout是函数，则忽略timeout参数
   */
  // tslint:disable-next-line:max-line-length
  static async info(option, timeout = this.defaultProps.timeout, callBack) {
    // 只能存在一个Toast,
    // 隐藏之前的toast
    await this.hide(false)
    // 存储渲染元素
    const _renderNode = document.createElement('div')
    this._renderNodes.push(_renderNode)

    let props = this._getProps(option, timeout, callBack)
    // 渲染toast
    render(<ClassAnimate
      zIndex={props.zIndex}
      name="dui-toast_fade"
      duration={props.duration}
      timeOut={props.timeout}
      onExit={() => this.hide(false, props.onHide)}
      ref={n => n && Toast._classAnimateDom.push(n)}
    >
      <ToastInner {...props} />
    </ClassAnimate>, _renderNode)
  }

  render() {
    return <ToastInner {...this.props}/>
  }
}
