import React from 'react'
import cx from 'classnames'

function sleep(ms) {
  return new Promise(resolve => {
    setTimeout(resolve, ms)
  })
}

// 类名自动切换，常用于出入场动画效果
// ${name}-enter
// ${name}-enter-active
// ${name}-exit
// ${name}-exit-active
export default class ClassAnimate extends React.Component {
  static defaultProps = {
    name: '',
    className: '',
    timeOut: 0,
  }

  isUnmounted // 组件是否将销毁
  timeStamp // 时间戳

  constructor(props) {
    super(props)
    const { className } = props
    this.state = {
      classNames: className || '',
    }
    this.isUnmounted = false
  }

  // setState Promise
  _setState(param) {
    return new Promise(resolve => {
      if (this.isUnmounted) {
        resolve()
      } else {
        this.setState(param, resolve)
      }
    })
  }

  // 离开动画结束回调
  // fireExit,是否需要触发退出
  async exitHandler(fireExit) {
    const { className, name, duration, onExit } = this.props
    // 该方法调用时的时间戳
    const timeStamp = Date.now()
    const offset = timeStamp - this.timeStamp
    // 等待入场动画结束
    if (offset < duration) {
      await sleep(duration - offset)
    }
    if (!this.isUnmounted) {
      // 元素退出
      // ${name}-exit
      await this._setState({ classNames: cx(className, `${name}-exit`) })
      await sleep(0)
      // ${name}-exit-active
      await this._setState({ classNames: cx(this.state.classNames, `${name}-exit-active`) })
      await sleep(duration)
      // 清除动画类
      await this._setState({ classNames: className })
    }
    if (!this.isUnmounted && fireExit && onExit) {
      onExit(this)
    }
  }

  async componentDidMount() {
    const { className, name, duration, timeOut } = this.props
    // 元素进入
    // ${name}-enter
    await this._setState({ classNames: cx(className, `${name}-enter`) })
    await sleep(0)
    // ${name}-enter-active
    await this._setState({ classNames: cx(this.state.classNames, `${name}-enter-active`) })
    // 开始动画时候的时间戳
    this.timeStamp = Date.now()
    // 等待动画结束
    await sleep(duration)
    // 清除动画类
    await this._setState({ classNames: className })
    // 如果设了自动退出
    if (timeOut !== 0) {
      await sleep(timeOut)
      this.exitHandler(true)
    }
  }

  componentWillUnmount() {
    this.isUnmounted = true
  }

  render() {
    const { classNames } = this.state
    const { children, zIndex } = this.props
    let style = null
    if (zIndex) {
      style = {
        zIndex,
        position: 'absolute',
      }
    }
    return <div
      style={style}
      className={classNames}
    >
      {children}
    </div>
  }
}
