// 包装模态组件,如toast、modal、dialog等
// 使用createPortal，兼容React15+
import React from 'react'
import ReactDOM from 'react-dom'

// 手动渲染组件
export const render = function (
  node,
  renderNode,
  appendNode = document.body,
) {
  ReactDOM.render(node, renderNode)
  appendNode.appendChild(renderNode)
}

/**
 * 手动销毁组件
 * @param renderNode 被手动渲染(↑↑↑)插入生成的div
 */
export const destroy = function (renderNode) {
  const unmountResult = ReactDOM.unmountComponentAtNode(renderNode)
  if (unmountResult && renderNode.parentNode) {
    renderNode.parentNode.removeChild(renderNode)
    renderNode = null
  }
}

export default class ModalHelper extends React.Component {

  static defaultProps = {
    appendNode: document ? document.body : null,
    noUsePortal: false,
  }

  node
  canUsePortal
  createPortal

  constructor(props) {
    super(props)
    // 能否使用React Portal,如不能用，降级为ReactDOM.unstable_renderSubtreeIntoContainer
    // ReactDOM.unstable_renderSubtreeIntoContainer(parent, component, dom)
    this.canUsePortal = ReactDOM.createPortal !== undefined && !props.noUsePortal
    this.createPortal = this.canUsePortal
      ? ReactDOM.createPortal
      : ReactDOM.unstable_renderSubtreeIntoContainer
  }

  // 手动渲染组件,如果不支持ReactDOM.createPortal的话
  renderPortal(props) {
    this.createPortal(
      this,
      props.children,
      this.node,
    )
  }

  // 组件卸载时，手动从DOM中移除this.node,保持页面整洁
  removePortal = () => {
    // tslint:disable-next-line:no-unused-expression
    !this.canUsePortal && ReactDOM.unmountComponentAtNode(this.node)
    this.props.appendNode.removeChild(this.node)
  }

  componentWillReceiveProps(nextProps) {
    // tslint:disable-next-line:no-unused-expression
    !this.canUsePortal && this.renderPortal(nextProps)
  }

  componentDidMount() {
    if (!this.canUsePortal) {
      this.node = document.createElement('div')
    }
    const { zIndex, className, appendNode } = this.props
    this.node.className = className || ''
    this.node.style.cssText = `position: absolute; ${zIndex ? 'z-index:' + zIndex : '' }`
    appendNode.appendChild(this.node)

    // 手动渲染组件
    // tslint:disable-next-line:no-unused-expression
    !this.canUsePortal && this.renderPortal(this.props)
  }

  // 组件卸载时移除node
  componentWillUnmount() {
    if (!this.node) {
      return
    }
    const delay = this.props.delay
    if (delay) {
      setTimeout(() => {
        this.removePortal()
      }, delay)
    } else {
      this.removePortal()
    }
  }

  render() {
    if (!this.canUsePortal) {
      return null
    }

    if (!this.node && this.canUsePortal) {
      this.node = document.createElement('div')
    }

    return this.createPortal(
      this.props.children,
      this.node,
    )
  }
}
