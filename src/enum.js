export const ICON_FIRE_HYDRANT = {
  ICON01: require('./images/icon_fire_hydrant01.png'),
  ICON02: require('./images/icon_fire_hydrant02.png'),
  ICON03: require('./images/icon_fire_hydrant03.png'),
  ICON04: require('./images/icon_fire_hydrant04.png'),
  ICON05: require('./images/icon_fire_hydrant05.png'),
  ICON06: require('./images/icon_fire_hydrant06.png'),
  ICON07: require('./images/icon_fire_hydrant07.png'),
  ICON08: require('./images/icon_fire_hydrant08.png'),
  ICON09: require('./images/icon_fire_hydrant09.png'),
}

export const TEAM_TYPE_ENUM = {
  1: '城市政府专职消防队',
  2: '乡镇政府专职消防队',
  3: '乡镇政府志愿消防队',
  4: '企业专职消防队',
  5: '社区微型消防队',
  6: '重点单位微型消防队',
  7: '民间救援队',
  8: '混编制执勤中队',
  9: '其他类型'
}

export const GLOBAL_DATA_ENUM = {
  __TEAM_DATA: '__TEAM_DATA', // 全局队伍数据
  __TEAM_ZHIDUI: '__TEAM_ZHIDUI', // 全局支队队伍
  __TEAM_DADUI: '__TEAM_DADUI', // 全局大队队伍
}

// 地图层级范围
export const MAP_ZOOM_ENUM = {
  ZHIDUI: 9, // 小于该范围的一律展示支队
  TEAMS: 10, // 大于等于该层级的展示具体队伍
  LEVEL01: 12, // 点击支队后展示到的层级
  LEVEL02: 14, // 点击大队后展示到的层级
}
