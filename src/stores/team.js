// 支队信息
export default {
  "绍兴支队": {
    longitude: '120.554072',
    latitude: '29.996988'
  },
  "台州支队": {
    longitude: '121.427391',
    latitude: '28.645095'
  },
  "杭州支队": {
    longitude: '120.203334',
    latitude: '30.23331'
  },
  "温州支队": {
    longitude: '120.727412',
    latitude: '27.999928'
  },
  "义乌支队": {
    longitude: '120.070019',
    latitude: '29.334329'
  },
  "宁波支队": {
    longitude: '121.531459',
    latitude: '29.89222'
  },
  "嘉兴支队": {
    longitude: '120.751766',
    latitude: '30.722498'
  },
  "丽水支队": {
    longitude: '119.907119',
    latitude: '28.470978'
  },
  "金华支队": {
    longitude: '119.713583',
    latitude: '29.098225'
  },
  "衢州支队": {
    longitude: '118.83686053',
    latitude: '28.96215407'
  },
  "湖州支队": {
    longitude: '120.083048',
    latitude: '30.904322'
  },
  "舟山支队": {
    longitude: '122.205838',
    latitude: '29.991184'
  },
}