import TeamForZhidui from '../stores/team';

// 获取数据中所有有效数据的消防队
export function getAllTeams(data = []) {
  let teams = [];
  for (let team of data) {
    if (team.sons && team.sons.length > 0) {
      team.children = team.sons
      teams = teams.concat(getAllTeams(team.sons))
    } else if (team.longitude && team.latitude && +team.longitude >= +team.latitude) {
      teams.push(team);
    }
  }
  return teams;
}

/**
 * 统计支队数据
 */
export function getZhiduiData(teams = []) {
  const teamsTmp = [];
  for (const team of teams) {
    if (team.name.match(/支队$/)) {
      const match = TeamForZhidui[team.name];
      if (match) {
        const children = getAllTeams(team.sons);
        teamsTmp.push({
          id: team.id,
          name: team.name,
          longitude: match.longitude,
          latitude: match.latitude,
          amount: children.length,
          children
        })
      }
    }
  }
  return teamsTmp;
}
